var express = require('express'),
    app = express();
var port = 8004;

var shell = require('shelljs')

app.listen(port);

var bodyParser = require('body-parser');
var main = require('./controller/main');
var sendTextMessage = require('./controller/api/sendTextMessage')
var config = require('./config.json')

app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.end('wellbot connected.')
});

app.get('/webhook/', function(req, res) {
    if (req.query['hub.verify_token'] === config.token_webhook) {
        res.send(req.query['hub.challenge'])
    } else
        res.send('Error,wrong token')
})

app.post('/gitupdate', (req, res) => {
    console.log('[' + new Date() + '] GIT PULL ORIGIN ........................................')
    shell.cd('/home/dev/www/wellbot/')
    shell.exec('git pull origin master')
    console.log('[' + new Date() + '] NPM INSTALL .............................')
    shell.exec('npm install --save')
    console.log('[' + new Date() + '] RESTARTING APP .............................')
    shell.exec('pm2 restart wellbot')
    console.log('[' + new Date() + '] Webhook bitbuckets Received and Updated...............')
    res.json({ response: 'webhook bitbuckets received successfully..........' })
})


app.post('/webhook/', function(req, res) {
    var data = req.body;

    // Make sure this is a page subscription
    if (data.object === 'page') {

        // Iterate over each entry - there may be multiple if batched
        data.entry.forEach(function(entry) {
            var pageID = entry.id;
            var timeOfEvent = entry.time;

            // Iterate over each messaging event
            entry.messaging.forEach(function(event) {
                if (event.message && event.sender.id !== config.pageId)
                    receivedMessage(event)
                else if (event.postback && event.sender.id !== config.pageId)
                    postbackMessage(event)
                    //else
                    // console.log("Webhook received unknown event: ", event);
            });
        });


        // Assume all went well.
        //
        // You must send back a 200, within 20 seconds, to let us know
        // you've successfully received the callback. Otherwise, the request
        // will time out and we will keep trying to resend.
        res.sendStatus(200);
    }
});

var PAYLOAD = false

function receivedMessage(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfMessage = event.timestamp;
    var message = event.message;

    var messageId = message.mid;

    var messageText = message.text;

    var messageAttachments = message.attachments;

    if (event.message.quick_reply) {
        messageText = event.message.quick_reply.payload
        PAYLOAD = true
    }

    if (messageText && senderID !== config.pageId) {
        main.execute(senderID, messageText, PAYLOAD);
    } else if (messageAttachments) {
        if (messageAttachments[0].type === "image" || messageAttachments[0].type === "video") {
            new sendTextMessage(senderID).addText("^_^").execute()
        } else
            new sendTextMessage(senderID).addText("^_^").execute()

        //sendTextMessage.fn(senderID, messageAttachments);
    }
}

function postbackMessage(event) {
    var senderID = event.sender.id;
    var recipientID = event.recipient.id;
    var timeOfMessage = event.timestamp;
    var messageText = event.postback.payload;

    if (messageText && senderID !== config.pageId) {
        main.execute(senderID, messageText, PAYLOAD)
    }
}