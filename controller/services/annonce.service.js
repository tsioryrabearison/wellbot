let DB = require('./config.service')
let wellcoloc = new DB("wellcoloc.com", 27017, "wellcoloc", "", "")

let self = module.exports = {

    getList: type => new Promise((resolve, reject) => {
        wellcoloc.connect().then(db => {
            db.collection("annonce")
                .find({
                    type_annonce: type
                })
                .toArray((err, res) => {
                    if (err || res.length === 0)
                        reject(err)
                    else {
                        resolve(res)
                    }
                })
        })
    }),

    getDescription: item_id => new Promise((resolve, reject) => {
        wellcoloc.connect().then(db => {
            db.collection('annonce')
                .find({
                    id_annonce: item_id
                })
                .toArray((err, res) => {
                    if (err || res.length === 0)
                        reject(err)
                    else {
                        resolve(res[0].description_annonce)
                    }
                })
        })
    })
}