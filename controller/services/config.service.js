'use strict';

module.exports = class {

    constructor(host, port, dbName, user, password) {
        this.dbConnected = false;
        this.mongoDb = null;
        this.host = host;
        this.port = String(port);
        this.dbName = dbName;
        this.user = user;
        this.password = password;
        this.mongoClient = null;
    }

    connect() {
        let MongoClient = require('mongodb').MongoClient;
        this.mongoClient = MongoClient

        let promise = new Promise((resolve, reject) => {
            if (!this.dbConnected) {

                let config = `mongodb://${this.host}:${this.port}/${this.dbName}`

                MongoClient.connect(config, (err, database) => {
                    if (err) {
                        console.log(err)
                        return
                    }
                    console.log('++++++++++ DATABASE CONNECTED +++++++++')
                    this.dbConnected = true;
                    this.mongoDb = database.db(this.dbName);
                    resolve(this.mongoDb);
                })
            } else
                resolve(this.mongoDb)
        });

        return promise;
    }
}