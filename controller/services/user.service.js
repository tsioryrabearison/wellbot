let DB = require('./config.service')
let wellcoloc = new DB("wellcoloc.com", 27017, "wellcoloc", "", "")

let self = module.exports = {

    save: (DATA) => new Promise((resolve, reject) => {
        wellcoloc.connect().then(db => {
            db.collection("user").save(DATA, (err, res) => {
                if (err) {
                    console.log('++++++++++ ERROR USER INSERTION +++++++++')
                    reject(err)
                } else {
                    console.log('++++++++++ DATABASE USER UPDATED +++++++++')
                    resolve({ db: db, data: res })
                }
            })
        })
    }),

    update: (senderId, data) => new Promise((resolve, reject) => {
        wellcoloc.connect().then(db => {
            db.collection("user").update({ fbid: senderId }, data)
                .then(() => resolve())
                .catch(() => reject())
        })
    }),

    read: senderId => new Promise((resolve, reject) => {
        wellcoloc.connect().then(db => {
            db.collection("user")
                .find({
                    fbid: senderId
                })
                .toArray((err, res) => {
                    if (err || res.length === 0)
                        reject(err)
                    else {
                        resolve({ db: db, data: res[0] })
                    }
                })
        })
    }),

    addLogin: (pseudo, password, id_user) => new Promise((resolve, reject) => {
        wellcoloc.connect().then(db => {
            db.collection("login")
                .save({ pseudo: pseudo, password: password, id_user: id_user }, (err, res) => {
                    if (err) {
                        console.log('++++++++++ ERROR ADDING USER +++++++++')
                        reject(err)
                    } else {
                        console.log('++++++++++ USER ADDED +++++++++')
                        resolve(res)
                    }
                })
        })
    }),

    countCollection: collection => new Promise((resolve) => {
        wellcoloc.connect()
            .then(db => {
                resolve(db.collection(collection).find().count())
            })
    }),

    login: (pseudo, password) => new Promise(resolve => {
        wellcoloc.connect()
            .then(db => {
                db.collection('login')
                    .find({ $and: [{ pseudo: pseudo }, { password: password }] })
                    .toArray((err, res) => {
                        err || res.length === 0 ? resolve(false) : resolve(true)
                    })
            })
    }),

    checkLogin: id_user => new Promise((resolve) => {
        wellcoloc.connect()
            .then(db => {
                db.collection('login')
                    .find({ id_user: id_user })
                    .toArray((err, res) => {
                        err || res.length === 0 ? resolve(false) : resolve(true)
                    })
            })
    })
}