const callSendAPI = require('../api/callSendAPI')
const sendMsg = require('../api/sendTextMessage')
const checkLogin = require('../app/checkLogin')
const db = require('../services/user.service')
let user = {}

module.exports = (senderId) => {

    callSendAPI.getProfil(senderId)
        .then(data => {
            return new sendMsg(senderId)
                .addText(`Hi ${data.first_name} ${data.last_name} :)`)
                .execute()
                .then(() => {
                    checkLogin(senderId)
                        .then(() => {
                            db.read(senderId).then(_db => {
                                console.log(`user ${data.first_name} already in database`)
                            }).catch(err => {
                                // SAVE USER PROFIL TO DATABASE IF NOT EXIST

                                db.countCollection('user')
                                    .then(c => {
                                        user.id_user = c
                                        user.fbid = senderId
                                        user.firstName = data.first_name
                                        user.lastName = data.last_name
                                        user.pic_prof = [data.profile_pic]
                                        user.date_birth = ''
                                        user.city_birth = ''
                                        user.address_user = ''
                                        user.phone = ''
                                        user.mail = ''
                                        user.status_user = { connected: true }
                                        user.notification = { annonce: [], message: [] }
                                        user.date_inscription = new Date()
                                        user.session = {
                                            action: "default",
                                            index: 0,
                                            field: {}
                                        }

                                        db.save(user).then(() => console.log('user ' + user.firstName + ' saved.'))
                                            .catch(err => console.log('user not saved!', err))
                                    })
                            })
                        })
                })
        })
}