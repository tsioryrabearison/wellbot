const db = require('../services/user.service')
const thread_list = require('../app/thread_list')
let _checkLogin = require('../app/checkLogin')
const home = require('../app/home')
const sendMsg = require('../api/sendTextMessage')
let login = new thread_list()

login
    .addQuestion('Enter your login', 'text', 'login')
    .addQuestion('Enter your password', 'text', 'password')
    .next('HOME')


module.exports = (user, messageText) => {
    let session_idx = user.session.index
    let index = session_idx || 0

    if (index > login.thread.length - 1) {
        user.session.field[login.thread[index - 1].key] = messageText
        db.update(user.fbid, {
            $set: {
                'session.field': user.session.field,
                'session.action': login.nextThread,
                'session.index': 0
            }
        }).then(() => {
            db.login(user.session.field.login, user.session.field.password)
                .then(res => {
                    db.update(user.fbid, {
                        $set: { session: { action: 'HOME', index: 0, field: {} } }
                    }).then(() => {
                        if (res) {
                            home(user.fbid)
                        } else {
                            new sendMsg(user.fbid)
                                .addText('Error login !')
                                .execute()
                                .then(() => _checkLogin(user.fbid))
                        }
                    })
                })
        })
    } else {

        if (index > 0)
            user.session.field[login.thread[index - 1].key] = messageText

        db.update(user.fbid, {
            $set: {
                'session.action': 'LOGIN',
                'session.field': user.session.field,
                'session.index': index + 1
            }
        }).then(() => login.fbid(user.fbid).execute(index))
    }
}