const db = require('../services/user.service')
const thread_list = require('../app/thread_list')
const home = require('../app/home')
const sendMsg = require('../api/sendTextMessage')
let createAccount = new thread_list()

createAccount
    .addQuestion('Enter your new login', 'text', 'login')
    .addQuestion('Enter your new password', 'text', 'password')
    .next('HOME')


module.exports = (user, messageText) => {
    let session_idx = user.session.index
    let index = session_idx || 0

    if (index > createAccount.thread.length - 1) {
        user.session.field[createAccount.thread[index - 1].key] = messageText
        db.update(user.fbid, {
            $set: {
                'session.field': user.session.field,
                'session.action': createAccount.nextThread,
                'session.index': 0
            }
        }).then(() => {
            new sendMsg(user.fbid)
                .addText('Congratulation, you are now a welluser! You can use this login and password to connect on https://www.wellcoloc.com')
                .execute()
                .then(() => {
                    db.addLogin(user.session.field.login, user.session.field.password, user.id_user)
                        .then(() => {
                            db.update(user.fbid, {
                                $set: { session: { action: 'HOME', index: 0, field: {} } }
                            }).then(() => { home(user.fbid) })
                        })
                })
        })
    } else {

        if (index > 0)
            user.session.field[createAccount.thread[index - 1].key] = messageText

        db.update(user.fbid, {
            $set: {
                'session.action': 'CREATE_ACCOUNT',
                'session.field': user.session.field,
                'session.index': index + 1
            }
        }).then(() => createAccount.fbid(user.fbid).execute(index))
    }
}