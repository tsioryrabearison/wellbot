const carousel = require('../api/sendCarousel')
const dbAnnonce = require('../services/annonce.service')

module.exports = (senderId, type) => {
    let counter = 0
    console.log("---> TYPE ", type)
    dbAnnonce.getList(type)
        .then(data => {
            let list = new carousel()
            data.forEach(item => {
                counter++
                list
                    .addButton('Interested', `action=INTERESTED&item_id=${item.id_annonce}`)
                    .addButton('Follow', `action=FOLLOW&item_id=${item.id_annonce}`)
                    .addButton('Show description', `action=DESCRIPTION&item_id=${item.id_annonce}`)
                    .addTemplate(
                        `${item.type_annonce} for ${item.price_annonce} €`,
                        `${item.address_annonce}\n${item.description_annonce}`,
                        item.pic_annonce[0]
                    )
                if (counter === data.length) {
                    list.fbid(senderId).execute()
                }
            })
        })
}