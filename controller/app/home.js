const quickMsg = require('../api/quickReply')

module.exports = (senderId) => {
    return new quickMsg(senderId)
        .addText('What do you want please?')
        .addButton('Location', 'action=LOCATION')
        .addButton('Colocation', 'action=COLOCATION')
        .execute()
}