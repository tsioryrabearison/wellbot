const db = require('../services/user.service')
const thread_list = require('../app/thread_list')
let search = new thread_list()
let card = require('./card')

let typeOfHouse = ['Appartment', 'Villa']
let category = ['Location', 'Colocation']

search
    .addQuestion('Enter an address', 'text', 'address')
    .addQuestion('price (€) ?', 'text', 'price')
    .addQuestion('Type of house', 'buttons', 'type')
    .option(typeOfHouse)
    .addQuestion('Categories', 'buttons', 'category')
    .option(category)
    .next('HOME')


module.exports = (user, messageText) => {
    let session_idx = user.session.index
    let index = session_idx || 0

    if (index > search.thread.length - 1) {
        user.session.field[search.thread[index - 1].key] = messageText
        db.update(user.fbid, {
            $set: {
                'session.field': user.session.field,
                'session.action': search.nextThread,
                'session.index': 0
            }
        }).then(() => {
            card(user.fbid, user.session.field.category)
        })
    } else {

        if (index > 0)
            user.session.field[search.thread[index - 1].key] = messageText

        db.update(user.fbid, {
            $set: {
                'session.action': 'SEARCH',
                'session.field': user.session.field,
                'session.index': index + 1
            }
        }).then(() => search.fbid(user.fbid).execute(index))
    }
}