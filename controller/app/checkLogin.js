const quickMsg = require('../api/quickReply')

module.exports = senderId => {
    return new quickMsg(senderId)
        .addText('Are you already a Welluser?')
        .addButton('Yes', 'action=LOGIN')
        .addButton('No, create account', 'action=CREATE_ACCOUNT')
        .execute()
}