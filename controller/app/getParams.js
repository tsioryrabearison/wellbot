module.exports = str => {
    return new Promise((resolve, reject) => {
        let param = str.split('&')
        let data = {}

        for (let k = 0; k < param.length; k++) {
            let tmp = param[k].split('=')
            data[tmp[0]] = tmp[1]
        }

        resolve(data)
    })
}