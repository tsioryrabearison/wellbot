var sendMsg = require('../api/sendTextMessage')
var quickMsg = require('../api/quickReply')
var buttonMsg = require('../api/sendWithButton')

module.exports = class {

    constructor() {
        this.senderId = null
        this.options = []
        this.thread = []
        this.nextThread = ""
    }

    fbid(senderId) {
        this.senderId = senderId
        return this
    }

    addQuestion(question, type, returnKey) {

        this.thread.push({
            question: question,
            type: type,
            option: null,
            key: returnKey
        })

        return this
    }

    next(thread_name) {
        this.nextThread = thread_name
        return this
    }

    option(buttons) {
        let thread = this.thread[this.thread.length - 1]

        this.thread[this.thread.length - 1].option = {
            action: () => {
                let th = thread.type === "quick_reply" ?
                    new quickMsg(this.senderId) :
                    new buttonMsg(this.senderId)

                th.addText(thread.question)

                if (buttons && buttons.length > 0) {
                    buttons.forEach(b => {
                        if (typeof b === "string") th.addButton(b)
                        else {
                            if (b.length === 1) th.addButton(b)
                            else if (b.length === 2) th.addButton(b[0], b[1])
                            else th.addButton(b[0], b[1], b[2])

                        }
                    })
                }

                return th.execute()
            }
        }

        return this
    }

    execute(index) {
        let thread = this.thread[index]

        if (thread) {
            if (thread.option) {
                return thread.option.action()
            } else {
                return new sendMsg()
                    .fbid(this.senderId)
                    .addText(thread.question)
                    .execute()
            }
        }
    }
}