const dbAnnonce = require('../services/annonce.service')
const sendMsg = require('../api/sendTextMessage')

module.exports = (senderId, item_id) => {
    dbAnnonce.getDescription(parseInt(item_id) || 0)
        .then(d => {
            new sendMsg(senderId)
                .addText(d)
                .execute()
        })
}