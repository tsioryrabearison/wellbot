var _callSendAPI = require('./callSendAPI.js');

module.exports = class {

    constructor(senderId) {
        this.senderId = senderId
    }

    fbid(fbid) {
        this.senderId = fbid
        return this
    }

    addText(text) {
        this.text = text
        return this
    }

    execute() {
        var messageData = {
            recipient: {
                id: this.senderId
            },
            message: {
                text: this.text
            }
        }
        return _callSendAPI.execute(messageData)
    }
}