var _callSendAPI = require('./callSendAPI.js');

module.exports = class {

    constructor(senderId) {
        this.senderId = senderId
        this.text = ""
        this.buttons = []
    }

    fbid(fbid) {
        this.senderId = fbid
        return this
    }

    addText(text) {
        this.text = text
        return this
    }

    addButton(title, payload) {

        this.buttons.push({
            type: "postback",
            title: title,
            payload: payload || title
        })

        return this
    }

    execute() {
        var messageData = {
            recipient: {
                id: this.senderId
            },
            message: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: "button",
                        text: this.text,
                        buttons: this.buttons
                    }
                }
            }
        }

        return _callSendAPI.execute(messageData)
    }
}