const _callSendAPI = require("./callSendAPI.js")

module.exports = class {

    constructor(senderId) {
        this.senderId = senderId
        this.buttons = []
        this.text = null
        this.attachment = {}
    }

    fbid(fbid) {
        this.senderId = fbid
        return this
    }

    addText(text) {
        this.text = text
        return this
    }

    addAttachment(type, url) {
        this.attachment = {
            type: type,
            payload: { url: url }
        }
        return this
    }

    addButton(title, payload, image_url) {
        let button = {
            type: "postback",
            title: String(title),
            payload: payload || title
        }

        if (image_url)
            button.image_url = image_url

        this.buttons.push(button)

        return this
    }

    execute() {
        var result = []

        for (var k = 0; k < this.buttons.length; k++) {

            let dt = {
                content_type: "text",
                title: this.buttons[k],
                payload: this.buttons[k]
            }

            if (this.buttons[k].payload) {
                dt.title = this.buttons[k].title
                dt.payload = this.buttons[k].payload
            }

            if (this.buttons[k].image_url)
                dt.image_url = this.buttons[k].image_url

            result.push(dt)
        }

        var messageData = {
            recipient: {
                id: this.senderId
            },
            message: {
                quick_replies: result
            }
        }

        if (this.text)
            messageData.message.text = this.text
        else
            messageData.message.attachment = this.attachment

        return _callSendAPI.execute(messageData)
    }
}