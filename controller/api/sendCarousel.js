let _callSendAPI = require('./callSendAPI.js');

module.exports = class {

    constructor(senderId) {
        this.senderId = senderId
        this.template = []
        this.buttons = []
    }

    fbid(fbid) {
        this.senderId = fbid
        return this
    }

    addButton(title, payload) {

        this.buttons.push({
            type: "postback",
            title: title,
            payload: payload || title
        })
        return this
    }

    addButonWeb_url(title, url) {

        this.buttons.push({
            type: "postback",
            title: title,
            url: url
        })

        return this
    }

    addTemplate(title, subtitle, image_url, default_action) {

        let template = { title: title }

        if (subtitle)
            template.subtitle = subtitle

        if (this.buttons.length > 0)
            template.buttons = this.buttons

        if (image_url)
            template.image_url = image_url

        if (default_action) {
            template.default_action = {
                type: "web_url",
                url: default_action,
                webview_height_ratio: "tall"
            }
        }

        this.template.push(template)
        this.buttons = []

        return this
    }

    execute() {
        let messageData = {
            recipient: {
                id: this.senderId
            },
            message: {
                attachment: {
                    type: "template",
                    payload: {
                        template_type: "generic",
                        elements: this.template
                    }
                }
            }
        }

        return _callSendAPI.execute(messageData)
    }
}