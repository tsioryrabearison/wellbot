var request = require('request')
const config = require('../../config.json')
const token = config.token_app

let self = module.exports = {

    execute: messageData => new Promise((resolve, reject) => {
        request({
            uri: 'https://graph.facebook.com/v2.6/me/messages',
            qs: { access_token: token },
            method: 'POST',
            json: messageData

        }, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                //var recipientId = body.recipient_id;
                //var messageId = body.message_id;
                console.log("Message sent.................................................");
                resolve()

            } else {
                console.error("Unable to send message.");
                console.error(body);
                reject(error)
            }
        });
    }),

    getProfil: userID => new Promise((resolve, reject) => {
        request("https://graph.facebook.com/v2.6/" + userID + "?fields=first_name,last_name,profile_pic&access_token=" + token,
            (err, res, body) => { body ? resolve(JSON.parse(body)) : reject(err) })
    })
}