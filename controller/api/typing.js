var _callSendAPI = require('./callSendAPI.js');

module.exports = class {

    constructor(senderId) {
        this.senderId = senderId
        this.messageData = {
            recipient: {
                id: this.senderId
            }
        }
    }

    fbid(fbid) {
        this.senderId = fbid
        return this
    }

    on() {
        this.messageData.sender_action = "typing_on"
        return _callSendAPI.execute(this.messageData)
    }

    off() {
        this.messageData.sender_action = "typing_off"
        return _callSendAPI.execute(this.messageData)
    }
}