var _callSendAPI = require('./callSendAPI.js');

module.exports = class {

    constructor(senderId) {
        this.senderId = senderId
        this.url = null
        this.type = null
    }

    fbid(fbid) {
        this.senderId = fbid
        return this
    }

    addFile(url, type) {
        this.url = url
        this.type = type
        return this
    }

    execute() {
        var messageData = {
            recipient: {
                id: this.senderId
            },
            message: {
                attachment: {
                    type: this.type,
                    payload: {
                        url: this.type
                    }
                }
            }
        }

        return _callSendAPI.execute(messageData)
    }
}