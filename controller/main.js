const eventEmitter = require('events')
const event = new eventEmitter()

const quickMsg = require('./api/quickReply')

const greeting = require('./app/greeting')
const login = require('./app/login')
const createAccount = require('./app/createAccount')
const home = require('./app/home')
const card = require('./app/card')
const showDescription = require('./app/showDescription')
const interested = require('./app/interested')
const search = require('./app/search')
const follow = require('./app/follow')
const help = require('./app/help')
const about = require('./app/about')

const getParams = require('./app/getParams')

let dbUser = require('./services/user.service')

let default_message = new quickMsg()
    .addText("Sorry, I don't know what you mean.")
    .addButton("Home", "action=HOME")

let default_session = { action: 'HOME', index: 0, field: {} }

let eventList = [
    'GETTING_STARTED',
    'LOGIN',
    'CREATE_ACCOUNT',
    'LOCATION',
    'COLOCATION',
    'DESCRIPTION',
    'INTERESTED',
    'FOLLOW',
    'SEARCH',
    'HELP',
    'ABOUT',
    'HOME'
]

module.exports = {

    execute: (senderId, messageText) => {

        console.log('----------- MESSAGETEXT -------------', messageText, senderId)

        getParams(messageText).then(params => {

            dbUser.read(senderId).then(d => {
                if (!params.action)
                    params.action = d.data.session.action

                console.log('------ PARAMS : ', params)

                if (eventList.includes(params.action))
                    event.emit(params.action.toUpperCase(), { messageText: messageText, user: d.data, params: params })
                else
                    event.emit('UNKNOWN', { fbid: d.data.fbid })

            }).catch(e => {
                event.emit('GETTING_STARTED', {
                    messageText: messageText,
                    params: params,
                    user: {
                        fbid: senderId,
                        session: default_session
                    }
                })
            })
        })
    }
}

event.on('GETTING_STARTED', d => { greeting(d.user.fbid) })

event.on('LOGIN', d => { login(d.user, d.messageText) })

event.on('CREATE_ACCOUNT', d => { createAccount(d.user, d.messageText) })

event.on('HOME', d => { home(d.user.fbid) })

event.on('LOCATION', d => { card(d.user.fbid, 'location') })

event.on('COLOCATION', d => { card(d.user.fbid, 'colocation') })

event.on('DESCRIPTION', d => { showDescription(d.user.fbid, d.params.item_id) })

event.on('INTERESTED', d => { interested(d.user) })

event.on('FOLLOW', d => { follow(d.user) })

event.on('SEARCH', d => { search(d.user, d.messageText) })

event.on('HELP', d => { help(d.user) })

event.on('ABOUT', d => { about(d.user) })

event.on('UNKNOWN', d => { default_message.fbid(d.fbid).execute() })